import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Canvas extends JLabel{
	private static final long serialVersionUID = 1L;
	
	private static final int RAD = 10;
	private static final int VEL_X = 10;
	private static final int VEL_Y = 10;
	private static final int SIZE_X = 500;
	private static final int SIZE_Y = 500;
	
	private Vector<Integer> vecForRandNumbersX = new Vector<>();
	private Vector<Integer> vecForRandNumbersY = new Vector<>();
	private Random randomGenerator = new Random();
	
	private long reActivationTimeOfTimer;
	
	Vector<Cycle> vector = new Vector<Cycle>();
	
	private Square square;
	
	public long getReActivationTimeOfTimer() {
		return reActivationTimeOfTimer;
	}

	public void setReActivationTimeOfTimer(long reActivationTimeOfTimer) {
		this.reActivationTimeOfTimer = reActivationTimeOfTimer;
	}
	
	private Timer timer = new Timer();
	
	public void setTimer(Timer timer){
		this.timer=timer;
	}
	
	public Timer getTimer(){
		return this.timer;
	}
	
	TimerTask timerTask = new TimerTask(){
		public void run(){
			/*Move*/
			int X,Y;
			for(int i=0;i<vector.size();i++){
				if(vector.get(i).getDirection()=="UP"){
					Y=vector.get(i).getY();
					vector.get(i).setY(Y-VEL_Y);										
				}
				if(vector.get(i).getDirection()=="DOWN"){
					Y=vector.get(i).getY();
					vector.get(i).setY(Y+VEL_Y);
					
				}
				if(vector.get(i).getDirection()=="LEFT"){
					X=vector.get(i).getX();
					vector.get(i).setX(X-VEL_X);
					
				}
				if(vector.get(i).getDirection()=="RIGHT"){
					X=vector.get(i).getX();
					vector.get(i).setX(X+VEL_X);
					
				}
				vector.get(i).setPrevDirection(vector.get(i).getDirection());
				if(i==0)
					continue;
				vector.get(i).setDirection(vector.get(i-1).getPrevDirection());
			}
			
			
			int xHead,yHead;
			
			xHead=vector.get(0).getX();
			yHead=vector.get(0).getY();
			
			/*Eat*/			
			if(xHead==square.getX() && yHead==square.getY()){
				addCycle();
				System.out.println("Snake Size :"+" "+vector.size());
				square=new Square(vecForRandNumbersX.get(randomGenerator.nextInt(vecForRandNumbersX.size())),vecForRandNumbersY.get(randomGenerator.nextInt(vecForRandNumbersY.size())));
			}
			
			/*Die*/
			if(xHead>500 || yHead>500 || xHead<0 || yHead<0){
				vector.removeAllElements();
				initCanvas();
				System.out.println("You died !");
			}
			for(int i=1;i<vector.size();i++){
				if(xHead==vector.get(i).getX() && yHead==vector.get(i).getY()){
					vector.removeAllElements();
					initCanvas();
					System.out.println("You died !");
				}
			}
			
			/*Goal*/
			if(vector.size()==25){
				timer.cancel();
				JFrame jFrameW=new JFrame();
				jFrameW.setTitle("Winner");
				jFrameW.setSize(250,250);
				jFrameW.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				jFrameW.setResizable(false);
				jFrameW.setLocationRelativeTo(null);
				
				JLabel jLabel=new JLabel("You won !");
				
				jFrameW.add(jLabel);
				jFrameW.pack();
				
				jFrameW.setVisible(true);
			}
			
			repaint();
		}
	};
	
	public void addCycle(){
		int lastX,lastY;
		String lastPrevDirection;
		
		Cycle newCycle;
		
		lastX=vector.get(vector.size()-1).getX();
		lastY=vector.get(vector.size()-1).getY();
		lastPrevDirection=vector.get(vector.size()-1).getPrevDirection();
		
		if(lastPrevDirection=="UP"){
			newCycle = new Cycle(lastX,lastY+10,lastPrevDirection);
							
			vector.add(newCycle);
		}
		if(lastPrevDirection=="DOWN"){
			newCycle = new Cycle(lastX,lastY-10,lastPrevDirection);
			
			vector.add(newCycle);
		}
		if(lastPrevDirection=="LEFT"){
			newCycle = new Cycle(lastX+10,lastY,lastPrevDirection);
			
			vector.add(newCycle);
		}
		if(lastPrevDirection=="RIGHT"){
			newCycle = new Cycle(lastX-10,lastY,lastPrevDirection);
			
			vector.add(newCycle);
		}
	}
	
	public void timerSetUp(){
		timer.scheduleAtFixedRate(timerTask, 0, reActivationTimeOfTimer);
	}
	
	public void initCanvas(){
		
		vector.add(new Cycle(20,20,"RIGHT"));
		vector.add(new Cycle(10,20,"RIGHT"));
		vector.add(new Cycle(0,20,"RIGHT"));
		vector.add(new Cycle(-10,20,"RIGHT"));
		vector.add(new Cycle(-20,20,"RIGHT"));
		
		for(int i=0;i<SIZE_X-10*RAD;i+=10){
			vecForRandNumbersX.add(i);	
		}
		
		for(int i=0;i<SIZE_Y-10*RAD;i+=10){
			vecForRandNumbersY.add(i);	
		}
		
		square=new Square(vecForRandNumbersX.get(randomGenerator.nextInt(vecForRandNumbersX.size())),vecForRandNumbersY.get(randomGenerator.nextInt(vecForRandNumbersY.size())));
	
	}
		
	public void paint(Graphics g){	
		g.setColor(Color.GREEN);
		
		for(int i=0;i<vector.size();i++){
			g.fillOval(vector.get(i).getX(),vector.get(i).getY(),RAD,RAD);
		}
		
		g.setColor(Color.RED);
		
		g.fillRect(square.getX(),square.getY(),RAD,RAD);
	}
}
