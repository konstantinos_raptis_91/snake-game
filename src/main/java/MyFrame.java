import java.awt.Color;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;


public class MyFrame implements KeyListener{
	private Canvas canvas=new Canvas();
	private JFrame jFrame;

	public MyFrame(){
		jFrame=new JFrame();		
		jFrame.setTitle("Snake Game");
		jFrame.setSize(500,500);
		jFrame.getContentPane().setBackground(Color.BLACK);
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.setResizable(false);
		jFrame.setLocationRelativeTo(null);
				
		jFrame.addKeyListener(this);
		
		canvas.setReActivationTimeOfTimer(50);
		canvas.initCanvas();
		canvas.timerSetUp();
		
		jFrame.add(canvas);
		
		jFrame.setVisible(true);
	}

	@Override
	public void keyPressed(KeyEvent e){
		/*Change Head Direction*/
		if(e.getKeyCode()==KeyEvent.VK_UP){
			canvas.vector.get(0).setDirection("UP");
			
			System.out.println("UP");
		}
		if(e.getKeyCode()==KeyEvent.VK_DOWN){
			canvas.vector.get(0).setDirection("DOWN");
			
			System.out.println("DOWN");
		}
		if(e.getKeyCode()==KeyEvent.VK_LEFT){
			canvas.vector.get(0).setDirection("LEFT");
			
			System.out.println("LEFT");
		}
		if(e.getKeyCode()==KeyEvent.VK_RIGHT){
			canvas.vector.get(0).setDirection("RIGHT");
			
			System.out.println("RIGHT");
		}
	}

	@Override
	public void keyReleased(KeyEvent e){	
	}

	@Override
	public void keyTyped(KeyEvent e){	
	}
}
