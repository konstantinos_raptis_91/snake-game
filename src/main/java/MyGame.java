import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyGame {
	
	JFrame menu=new JFrame();
	
	public MyGame(){
		menu.setTitle("Snake Game Menu");
		menu.setSize(350,100);
		menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menu.getContentPane().setBackground(Color.BLACK);
		menu.setResizable(false);
		menu.setLocationRelativeTo(null);
		
		JPanel jPanel=new JPanel();
		jPanel.setOpaque(false);
		jPanel.setLayout(new FlowLayout());
		
		JButton jButtonStart=new JButton("New Game");
		jButtonStart.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				new MyFrame();
				menu.dispose();
			}
		});
		
		JButton jButtonExit=new JButton("Exit");
		jButtonExit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				menu.dispose();
			}
		});
		
		jPanel.add(jButtonStart);
		jPanel.add(jButtonExit);
		
		menu.add(jPanel);

		menu.setVisible(true);
	}
	
	public static void main(String[] args){	
		new MyGame();
	}
}
